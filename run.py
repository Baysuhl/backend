import discord
import ujson
import aiohttp_cors
import gspread
from dotenv import load_dotenv
from aiohttp import web
from aiohttp_session import setup
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from oauth2client.service_account import ServiceAccountCredentials

import os
import asyncio
import base64
import logging
import platform

from data import db
from data.points import UserPoints
from data.crowns import Crowns
from routes import routes
from bot import FurryHelper
from client import Client

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

# Disable logging from GINO and asyncio
logging.getLogger('asyncio').propagate = False
logging.getLogger('gino').propagate = False
logging.getLogger('urllib3').propagate = False
logging.getLogger('oauth2client').propagate = False
logger = logging.getLogger('web')
logger.setLevel(logging.INFO)

IS_NOT_WINDOWS = not platform.system() == 'Windows'

load_dotenv(override=True)

app = web.Application()
app.add_routes(routes)

# Signed cookie setup
fernet_key = os.getenv('SECRET_KEY')
secret_key = base64.urlsafe_b64decode(fernet_key)
setup(app, EncryptedCookieStorage(secret_key, max_age=7200, cookie_name='nuzzles', secure=IS_NOT_WINDOWS,
                                  encoder=ujson.dumps, decoder=ujson.loads))

# CORS setup
cors = aiohttp_cors.setup(app, defaults={
    '*': aiohttp_cors.ResourceOptions(
        allow_credentials=True,
        expose_headers='*',
        allow_headers='*',
        allow_methods=['POST', 'GET']
    )
})

for route in app.router.routes():
    cors.add(route)

runner = web.AppRunner(app, access_log=None)


async def level_leaderboard():
    bot = app['bot']
    await bot.wait_until_ready()

    while True:
        try:
            position = 1

            async with db.transaction():
                async for user in UserPoints.query.order_by(UserPoints.points.desc()).gino.iterate():
                    member = bot.guild.get_member(user.user_id)

                    if member is None or member.bot:
                        continue

                    try:
                        level = max(level for level, points in app['curve'].items() if points <= user.points)
                    except ValueError:
                        level = 0

                    app['levels'][user.user_id] = (position, level, user.points)
                    position += 1

        except (AttributeError, ValueError):
            # Something went wrong, but let's try again next time.
            pass

        finally:
            await asyncio.sleep(240)  # 4 minutes


async def crowns_leaderboard():
    bot = app['bot']
    await bot.wait_until_ready()

    while True:
        try:
            position = 1

            async with db.transaction():
                async for user in Crowns.query.order_by(Crowns.crowns.desc()).gino.iterate():
                    member = bot.guild.get_member(user.user_id)

                    if member is None or member.bot:
                        continue

                    app['crowns'][user.user_id] = (position, user.crowns)
                    position += 1

        except (AttributeError, ValueError):
            # Something went wrong, but let's try again next time.
            pass

        finally:
            await asyncio.sleep(210)  # 3 1/2 minutes


async def startup(_):
    intents = discord.Intents.all()
    bot = FurryHelper(command_prefix='_', intents=intents)
    app['bot'] = bot

    asyncio.create_task(bot.start(os.getenv('BOT_TOKEN')))
    logger.info('Started bot instance.')

    # Start leaderboard tasks
    curve = {}

    def _exp(level: int):
        if level == 0:
            return 0

        prev_level = curve.get(level - 1, None)

        if prev_level is None:
            prev_level = _exp(level - 1)

        return prev_level + ((5 * level * level) + 50 * level + 100)

    app['curve'] = {
        level: _exp(level) for level in range(1, 101)
    }

    logger.info('Loading level leaderboard.. This may take some time.')

    app['levels'] = {}
    asyncio.create_task(level_leaderboard())

    app['crowns'] = {}
    asyncio.create_task(crowns_leaderboard())

    if IS_NOT_WINDOWS:
        unix_path = os.getenv('IRIS_UNIX')
        reader, writer = await asyncio.open_unix_connection(unix_path)
    else:
        tcp = os.getenv('IRIS_WINDOWS').split(':')
        host, port = tcp[0], int(tcp[1])

        reader, writer = await asyncio.open_connection(host=host, port=port)

    client = Client(reader, writer)
    app['client'] = client


async def spreadsheet(_):
    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']

    credentials = ServiceAccountCredentials.from_json_keyfile_name('Royal Helper-eff1dda37b5e.json', scope)

    gc = await loop.run_in_executor(None, gspread.authorize, credentials)
    _spreadsheet = await loop.run_in_executor(None, gc.open, 'Furry Roayle Website Messages')

    app['sheet'] = _spreadsheet.sheet1
    app['gspread'] = gc
    app['refresh_sheet'] = spreadsheet


async def shutdown(_):
    logger.info('Closing bot connection.')
    bot = app['bot']
    await bot.logout()

    # Close Gino.
    logger.info('Closing database connection.')
    await db.pop_bind().close()


app.on_startup.extend([startup, spreadsheet])
app.on_shutdown.append(shutdown)


async def main():
    uri = os.getenv('DATABASE_URI')
    await db.set_bind(uri)
    logger.info('Successfully connected to the database.')

    await runner.setup()
    site = web.UnixSite(runner, '/run/uvicorn.sock') if IS_NOT_WINDOWS else web.TCPSite(runner, '10.0.0.99', 8000)
    await site.start()


if __name__ == '__main__':
    try:
        import uvloop

        uvloop.install()
        logger.info('Running with ultra-violet loop.')
    except ModuleNotFoundError:
        pass

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

    try:
        logger.info('Running site, bro~!')
        loop.run_forever()
    except KeyboardInterrupt:
        logger.info('Shutting down gracefully..')
        loop.run_until_complete(runner.cleanup())
