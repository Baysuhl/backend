import os
import logging

from dotenv import load_dotenv, find_dotenv
from discord.ext import commands

logging.getLogger('discord').setLevel(logging.WARN)
logging.getLogger('websockets').propagate = False


class FurryHelper(commands.Bot):

    def __init__(self, command_prefix, **options):
        super().__init__(command_prefix, **options)

        # load_dotenv(find_dotenv())

        self.logger = logging.getLogger('bot')
        self.guild = None

    async def on_ready(self):
        self.logger.info(f'Bot has connected and identified as {self.user}.')

        _id = int(os.getenv('GUILD_ID'))
        self.guild = self.get_guild(_id)
        self.logger.debug(f'Got guild object for {self.guild.name}.')

        if self.guild.large:
            await self.request_offline_members(self.guild)
