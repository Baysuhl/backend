import os
import platform
import asyncio
import logging

import ujson

IS_NOT_WINDOWS = not platform.system() == 'Windows'


class Client:

    delimiter = b'\0'
    __slots__ = ('reader', 'writer', 'loop', 'logger')

    def __init__(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        loop = asyncio.get_running_loop()

        self.loop = loop
        self.reader = reader
        self.writer = writer
        self.logger = logging.getLogger('client')
        self.logger.setLevel(logging.DEBUG)

        self.loop.create_task(self.keepalive())

    async def keepalive(self):
        self.logger.info('Keeping Iris connection alive.')
        while not self.writer.is_closing():
            try:
                data = await self.reader.readuntil(separator=self.delimiter)
                decoded = data.decode()
                self.logger.debug(f'Received data: {decoded}')
            except (ConnectionResetError, asyncio.IncompleteReadError):
                self.writer.close()
                self.logger.info('Connection to the Iris server has been closed. Attempting to reconnect.')
            except BaseException as err:
                self.logger.exception(err.__traceback__)

        self.loop.create_task(self.reconnect())

    async def reconnect(self):
        try:
            self.logger.info('Attempting reconnection to Iris server..')
            if IS_NOT_WINDOWS:
                unix_path = os.getenv('IRIS_UNIX')
                reader, writer = await asyncio.open_unix_connection(unix_path)
            else:
                tcp = os.getenv('IRIS_WINDOWS').split(':')
                host, port = tcp[0], int(tcp[1])

                reader, writer = await asyncio.open_connection(host=host, port=port)

            self.reader = reader
            self.writer = writer

            self.logger.info('Connection to Iris server re-established.')
            self.loop.create_task(self.keepalive())
        except ConnectionError:
            await asyncio.sleep(5)
            self.loop.create_task(self.reconnect())

    async def write(self, payload: dict):
        encoded = ujson.dumps(payload)
        data: bytes = encoded.encode() + self.delimiter

        self.writer.write(data)
        await self.writer.drain()

        self.logger.debug(f'Sent data: {encoded}')
