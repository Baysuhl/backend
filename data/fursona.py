from data import db


class Fursona(db.Model):
    __tablename__ = 'fursonas'

    discord_id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    gender = db.Column(db.String(50), nullable=False)
    age = db.Column(db.BigInteger, nullable=False)
    species = db.Column(db.Unicode, nullable=False)
    orientation = db.Column(db.String(50), nullable=False)
    height = db.Column(db.Integer, nullable=False)
    weight = db.Column(db.Integer, nullable=False)
    bio = db.Column(db.String(1024))
    image = db.Column(db.Text)
    verified = db.Column(db.Boolean, server_default=db.text('false'), nullable=False)
    colour = db.Column(db.Integer, server_default=db.text('0'), nullable=False)
    nsfw = db.Column(db.Boolean, server_default=db.text('false'), nullable=False)
    index = db.Column(db.SmallInteger, primary_key=True, server_default=db.text('1'), nullable=False)
    created = db.Column(db.DateTime, server_default=db.text('now()'), nullable=False)
    id = db.Column(db.Integer, server_default=db.text('nextval(\'fursonas_id_seq\'::regclass'))
