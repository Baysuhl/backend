from data import db


class Stats(db.Model):
    __tablename__ = 'stats'

    user_id = db.Column(db.BigInteger, primary_key=True)
    role = db.Column(db.BigInteger, nullable=False)
    timer_bumps = db.Column(db.Integer, server_default=db.text('0'), nullable=False)
    disboard_bumps = db.Column(db.Integer, server_default=db.text('0'), nullable=False)
    messages = db.Column(db.Integer, server_default=db.text('0'), nullable=False)
    gags = db.Column(db.Integer, server_default=db.text('0'), nullable=False)
    memes = db.Column(db.Integer, server_default=db.text('0'), nullable=False)
    bans = db.Column(db.Integer, server_default=db.text('0'), nullable=False)
    mail = db.Column(db.Integer, server_default=db.text('0'), nullable=False)
    period = db.Column(db.DateTime, server_default=db.text('date(now())'), nullable=False)
    links = db.Column(db.Integer, server_default=db.text('0'), nullable=False)
