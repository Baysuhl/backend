from data import db


class Verification(db.Model):
    __tablename__ = 'verification'

    user_id = db.Column(db.BigInteger, primary_key=True, nullable=False)
    expiration = db.Column(db.DateTime, nullable=False)
    address = db.Column(db.Text)
    key = db.Column(db.Text, unique=True)
    completed = db.Column(db.Boolean, server_default=db.text('false'), nullable=False)
    attempts = db.Column(db.SmallInteger, server_default=db.text('0'), nullable=False)
    linked = db.Column(db.Boolean, server_default=db.text('false'), nullable=False)

