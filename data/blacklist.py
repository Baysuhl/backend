from sqlalchemy.dialects.postgresql import CIDR
from data import db


class Blacklist(db.Model):
    __tablename__ = 'ip_blacklist'

    address = db.Column(CIDR, index=True, unique=True, nullable=False)
