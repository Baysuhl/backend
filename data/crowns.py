from data import db


class Crowns(db.Model):
    __tablename__ = 'crowns'

    user_id = db.Column(db.BigInteger, primary_key=True)
    crowns = db.Column(db.Integer)
    last_message = db.Column(db.DateTime)
    last_vc_join = db.Column(db.DateTime)
    titles = db.Column(db.SmallInteger)
