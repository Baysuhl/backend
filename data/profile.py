from data import db


class Profile(db.Model):
    __tablename__ = 'profiles'

    user_id = db.Column(db.BigInteger, primary_key=True)
    username = db.Column(db.Unicode)
    discriminator = db.Column(db.SmallInteger)
    nickname = db.Column(db.Unicode)
    avatar = db.Column(db.Text)
