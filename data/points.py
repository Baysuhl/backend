from data import db


class UserPoints(db.Model):
    __tablename__ = 'user_points'

    user_id = db.Column(db.BigInteger, primary_key=True)
    points = db.Column(db.Integer)
