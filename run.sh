if [ -f /run/uvicorn.sock ]; then
  sudo rm /run/uvicorn.sock
fi

screen -d -m -S backend sudo env "PATH=$PATH" python3 run.py

while [ ! -f /run/uvicorn.sock ]
do
  sleep 1
done

sudo chown www-data:www-data /run/uvicorn.sock && sudo chmod g+rwx /run/uvicorn.sock