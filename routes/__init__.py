import ujson
from aiohttp import web, ClientSession
from aiohttp_session import get_session, new_session
from asyncpg import PostgresError
from gino import GinoException
from sqlalchemy import and_, or_, func
from sqlalchemy.dialects.postgresql import insert
from dotenv import load_dotenv
from gspread import GSpreadException
from discord import DiscordException, Object as DiscordObject, Embed as DiscordEmbed, Colour as DiscordColor, \
    Member as DiscordMember, User as DiscordUser, TextChannel

import os
import logging
import typing
import itertools
import asyncio
from enum import IntEnum
from time import time as unix_timestamp
from datetime import datetime

from data import db
from data.fursona import Fursona
from data.profile import Profile
from data.verification import Verification
from data.blacklist import Blacklist
from data.stats import Stats

logger = logging.getLogger('web')

routes = web.RouteTableDef()

VALID_COLUMNS = {'name', 'age', 'gender', 'orientation', 'weight', 'height', 'nsfw'}
MAXIMUM_AGE = 100
MAXIMUM_HEIGHT = 96
MAXIMUM_WEIGHT = 400


class Orientation(IntEnum):
    Straight = 1
    Bisexual = 2
    Pansexual = 3
    Gay = 4


STRAIGHT_PATTERN = r'straight|hetero[-]?(sexual)?'
BISEXUAL_PATTERN = r'bi[-]?(sexual)?'
PANSEXUAL_PATTERN = r'pan[-]?(sexual)?'
GAY_PATTERN = r'gay|homo(sexual)?|les(bian)?'

ORIENTATION_PATTERNS = {
    Orientation.Straight: STRAIGHT_PATTERN,
    Orientation.Bisexual: BISEXUAL_PATTERN,
    Orientation.Pansexual: PANSEXUAL_PATTERN,
    Orientation.Gay: GAY_PATTERN
}


class Gender(IntEnum):
    Male = 1
    Female = 2
    FtM = 3
    MtF = 4
    NonBinary = 5


MALE_PATTERN = r'male|femboy'
FEMALE_PATTERN = r'female|girl'
TRANS_MALE_PATTERN = r'ftm|trans[-\s]?(male|man)|c([-]?(unt)?)boy'
TRANS_FEMALE_PATTERN = r'mtf|trans[-\s]?(woman|female)'
NON_BINARY_PATTERN = r'gender[-\s]?fluid|non[-\s]?binary'

GENDER_PATTERNS = {
    Gender.Male: MALE_PATTERN,
    Gender.Female: FEMALE_PATTERN,
    Gender.FtM: TRANS_MALE_PATTERN,
    Gender.MtF: TRANS_FEMALE_PATTERN,
    Gender.NonBinary: NON_BINARY_PATTERN
}

load_dotenv(override=True)

API_ENDPOINT = 'https://discordapp.com/api/v6'
CLIENT_ID = os.getenv('CLIENT_ID')
CLIENT_SECRET = os.getenv('CLIENT_SECRET')
REDIRECT_URI = os.getenv('REDIRECT_URI')
API_SCOPES = os.getenv('API_SCOPES')
BOT_TOKEN = os.getenv('BOT_TOKEN')
GUILD_ID = os.getenv('GUILD_ID')
WEBHOOK_URL = os.getenv('WEBHOOK_URL')
RECAPTCHA_SECRET = os.getenv('RECAPTCHA_SECRET')
VERIFICATION_ROLE = int(os.getenv('VERIFICATION_ROLE'))
VERIFICATION_CHANNEL = int(os.getenv('VERIFICATION_CHANNEL'))
MAX_VERIFICATION_ATTEMPTS = int(os.getenv('MAX_VERIFICATION_ATTEMPTS', 4))
NSFW_ROLE = int(os.getenv('NSFW_ROLE'))
MODMAIL_CHANNEL = int(os.getenv('MODMAIL_CHANNEL'))
NSFW_MODMAIL_CHANNEL = int(os.getenv('NSFW_MODMAIL_CHANNEL'))
INVIS_CAPTCHA_SECRET = '6LcJT80UAAAAAAGG8zns1MTw3t-80v6iNarAFLDj'


def _compile_filters(filters):
    _filters = []
    for column, values in filters.items():
        if column not in VALID_COLUMNS:
            logger.error('Received invalid column query!')
            continue  # Skip

        try:
            if isinstance(values, list):
                # Genders and orientations are provided as a list of strings
                if all(isinstance(value, str) for value in values):
                    if column == 'gender':
                        genders = [Gender(int(value)) for value in values]
                        _filter = []

                        for gender in genders:
                            try:
                                _filter.append(getattr(Fursona, column).op('~*')(GENDER_PATTERNS[gender]))
                            except KeyError:
                                logger.warning(f'Invalid gender option provided: {gender}')

                        _filters.append(or_(*_filter))

                        if Gender.Male in genders and Gender.Female not in genders:
                            _filters.append(Fursona.gender.notilike('female%'))

                        continue

                    elif column == 'orientation':
                        orientations = [Orientation(int(value)) for value in values]
                        _filter = []

                        for orientation in orientations:
                            try:
                                _filter.append(getattr(Fursona, column).op('~*')(ORIENTATION_PATTERNS[orientation]))
                            except KeyError:
                                logger.warning(f'Invalid gender option provided: {orientation}')

                        _filters.append(or_(*_filter))

                        continue

                if all(isinstance(value, int) for value in values):
                    minimum, maximum = values
                    # Only specify the minimum if the slider was maxed
                    if column == 'weight' and maximum >= MAXIMUM_WEIGHT or \
                            column == 'height' and maximum >= MAXIMUM_HEIGHT or \
                            column == 'age' and maximum >= MAXIMUM_AGE:
                        _filters.append(getattr(Fursona, column) >= minimum)
                        continue

                    _filters.extend((getattr(Fursona, column) >= minimum,
                                     getattr(Fursona, column) <= maximum))

                continue  # Don't bother evaluating the next lines

            if isinstance(values, str):
                _filters.append(getattr(Fursona, column).ilike(f'%{values}%'))

            if isinstance(values, bool):
                # Don't include the NSFW filter if the "Include NSFW" toggle is checked
                if not (column == 'nsfw' and values):
                    _filters.append(getattr(Fursona, column) == values)

        except (AttributeError, ValueError):
            logger.error(f'Received invalid filter query from user: {filters}')
            continue

    return _filters


@routes.post(r'/sonas/{page:\d+}')
async def sonas(request):
    try:
        page = int(request.match_info['page'])
    except ValueError:
        return web.json_response({'sonas': []}, status=400)

    if page < 1:
        page = 1

    offset = (page - 1) * 12

    filters = await request.json()

    if not filters or 'nsfw' not in filters:
        # Bad request.
        return web.json_response({'sonas': []}, status=400)

    logger.debug('Querying with filters: {}'.format(filters))

    _filters = _compile_filters(filters)

    query = await db.select(
        [Fursona.name, Fursona.bio, Fursona.image, Fursona.created, Profile.user_id, Profile.username,
         func.lpad(Profile.discriminator.cast(db.String), 4, '0').label('discriminator'), Profile.nickname,
         Profile.avatar]) \
        .where((and_(*_filters)) &
               (Fursona.verified == True) &
               (Profile.user_id == Fursona.discord_id)
               & Fursona.image.op('~')(r'(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|jpeg|gif|png)')) \
        .limit(12).offset(offset) \
        .order_by(Fursona.created.desc()) \
        .order_by(Fursona.id.desc()).gino.all()

    fursonas = [{
        'name': sona.name,
        'bio': sona.bio,
        'image': sona.image,
        'created': sona.created.timestamp(),
        'username': sona.username,
        'discriminator': sona.discriminator,
        'avatar': sona.avatar
    } for sona in query]

    return web.json_response({'sonas': fursonas}, dumps=ujson.dumps)


@routes.post('/sonas/count')
async def count(request):
    filters = await request.json()

    if not filters or 'nsfw' not in filters:
        # Bad request.
        return web.json_response({'count': 0}, status=400, dumps=ujson.dumps)

    _filters = _compile_filters(filters)

    _count = await db.select([db.func.count(Fursona.id)]) \
        .where((and_(*_filters)) &
               (Fursona.verified == True) &
               (Profile.user_id == Fursona.discord_id)
               & Fursona.image.op('~')(r'(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|jpeg|gif|png)')).gino.scalar()

    return web.json_response({'count': _count}, dumps=ujson.dumps)


@routes.get('/me/sonas')
async def mysonas(request):
    session = await get_session(request)

    if 'user' not in session:
        return web.json_response([], status=403)

    async with db.transaction():
        fursonas = [fursona.to_dict() async for fursona in
                    Fursona.query.where(Fursona.discord_id == int(session['user']['id'])).gino.iterate()]

        # Convert to UNIX because DateTime objects can't be serialized.
        for fursona in fursonas:
            fursona['created'] = fursona['created'].timestamp()

        return web.json_response(fursonas, dumps=ujson.dumps)


@routes.get('/me/age')
async def age(request):
    session = await get_session(request)

    if 'user' not in session:
        return web.json_response({'underage': True}, status=403)

    bot = request.app['bot']

    _id = int(session['user']['id'])
    member = bot.guild.get_member(_id)

    if member is None or not member._roles.has(NSFW_ROLE):
        return web.json_response({'underage': True}, status=403)

    return web.json_response({'underage': False})


@routes.get('/me')
async def me(request):
    # TODO: Keep the user object up-to-date
    session = await get_session(request)

    if 'user' not in session:
        return web.json_response({}, status=404, dumps=ujson.dumps)

    now = datetime.utcnow()
    exp = datetime.utcfromtimestamp(session['expiration'])

    if now > exp:
        # Token has expired, time to refresh it!
        data = {
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
            'grant_type': 'refresh_token',
            'refresh_token': session['refresh_token'],
            'redirect_uri': REDIRECT_URI,
            'scope': API_SCOPES
        }

        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        async with ClientSession() as request:
            response = await request.post(f'{API_ENDPOINT}/oauth2/token', data=data, headers=headers)
            data = await response.json(loads=ujson.loads)

            session['access_token'] = data['access_token']
            session['refresh_token'] = data['refresh_token']
            session['expiration'] = unix_timestamp() + data['expires_in']

            # Check if the user is still in the guild, if not, log them out.
            authorization_header = ' '.join((session['token_type'], session['access_token']))
            headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': authorization_header
            }

            guilds_response = await request.get(f'{API_ENDPOINT}/users/@me/guilds', headers=headers)
            guilds = await guilds_response.json(loads=ujson.loads)

            if not any([guild['id'] == GUILD_ID for guild in guilds]):
                session.invalidate()
                return web.json_response({}, status=404, dumps=ujson.dumps)

    return web.json_response(session['user'], dumps=ujson.dumps)


@routes.get('/logout')
async def logout(request):
    session = await get_session(request)

    if 'user' in session:
        data = {
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
            'token': session['access_token']
        }

        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }

        async with ClientSession(headers=headers) as request:
            response = await request.post(f'{API_ENDPOINT}/oauth2/token/revoke', data=data)

        session.invalidate()
        return web.json_response({})

    # Bad request
    return web.json_response({}, status=400)


@routes.post('/login')
async def login(request):
    data = await request.json()

    try:
        token = data['token']
    except KeyError:
        return web.json_response({}, status=400, dumps=ujson.dumps)

    session = await new_session(request)

    data = {
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'grant_type': 'authorization_code',
        'code': token,
        'redirect_uri': REDIRECT_URI,
        'scope': API_SCOPES
    }

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    async with ClientSession(json_serialize=ujson.dumps) as request:
        async with request.post(f'{API_ENDPOINT}/oauth2/token', data=data, headers=headers) as response:
            exchange = await response.json(loads=ujson.loads)

            try:
                access_token = exchange['access_token']
                refresh_token = exchange['refresh_token']
                token_type = exchange['token_type']  # Usually "Bearer" but just in case
                expiration = unix_timestamp() + exchange['expires_in']
            except KeyError:
                logger.error(f'Oof, error in exchange ({response.status}): {exchange}')
                return web.json_response({'error': 'We ran into an issue while processing your request.'},
                                         dumps=ujson.dumps)

            authorization_header = ' '.join((token_type, access_token))

            headers.update({'Authorization': authorization_header})

            # User object retrieval
            async with request.get(f'{API_ENDPOINT}/users/@me', headers=headers) as me_response:
                user = await me_response.json(loads=ujson.loads)
                session['user'] = user

                data = {
                    'access_token': access_token
                }

                headers = {
                    'Authorization': f'Bot {BOT_TOKEN}'
                }

                # Add them to the guild. If we can't (due to server limit bug), ensure they're already in FR.
                user_id = user['id']
                async with request.put(f'{API_ENDPOINT}/guilds/{GUILD_ID}/members/{user_id}',
                                       json=data, headers=headers) as join_response:
                    if join_response.status not in (201, 204):
                        headers = {'Authorization': authorization_header}
                        async with request.get(f'{API_ENDPOINT}/users/@me/guilds', headers=headers) as guilds_response:
                            guilds = await guilds_response.json(loads=ujson.loads)

                            if not any(guild['id'] == GUILD_ID for guild in guilds):
                                return web.json_response({'error': 'Sorry, but you must be a part of Furry Royale in '
                                                                   'order to login.'})

            session['access_token'] = access_token
            session['refresh_token'] = refresh_token
            session['token_type'] = token_type
            session['expiration'] = expiration

            return web.json_response(user, dumps=ujson.dumps)


async def get_member_by_id(bot, _id: int) -> typing.Union[DiscordUser, DiscordMember, None]:
    member = bot.guild.get_member(_id)

    if not member:
        try:
            member = await bot.fetch_user(_id)
            return member

        except DiscordException:
            return None

    return member


@routes.get('/check/{token}')
async def check(request):
    bot = request.app['bot']
    token = request.match_info['token']

    try:
        address = request.headers['CF-Connecting-IP']
    except KeyError:
        address = request.remote

    logger.debug(f'Received token check request from: {address} with token {token}')

    verification = await Verification.query.where((Verification.key == token) &
                                                  (Verification.completed == False)).gino.first()

    if not verification or datetime.utcnow() > verification.expiration:
        return web.json_response({'error': 'This link is valid or has expired. '
                                           'Please contact a staff member for a new one.'},
                                 status=400, dumps=ujson.dumps)

    verification_user = await get_member_by_id(bot, verification.user_id)

    if verification_user not in bot.guild.members:
        await verification.delete()
        return web.json_response({'error': 'You need to be in the server in order to verify.'},
                                 status=403, dumps=ujson.dumps)

    blacklisted = await db.select([func.count(Blacklist.address)]) \
        .where(Blacklist.address.op('>>=')(address)).gino.scalar()

    channel = bot.guild.get_channel(VERIFICATION_CHANNEL)

    # Log IP address
    await Verification.update.values(address=address).where(Verification.user_id == verification.user_id).gino.status()

    if blacklisted > 0:
        embed = DiscordEmbed(title='Bad IP Detected',
                             description='User tried to verify themselves while on a VPN or Tor.')
        embed.set_author(name=str(verification_user), icon_url=verification_user.avatar_url_as(format='png'))
        embed.add_field(name='User', value=verification_user.mention, inline=False)

        await channel.send(embed=embed)

        return web.json_response({'error': 'Please switch off your VPN or Tor before attempting the captcha.'},
                                 status=403, dumps=ujson.dumps)

    accounts = await db.select([Verification.user_id]) \
        .where((Verification.address == address) & (Verification.user_id != verification.user_id)).gino.all()

    # "If there is an original account."
    if len(accounts) > 1:
        await verification.delete()
        original = accounts[0]
        original_user = await get_member_by_id(bot, original.user_id)

        embed = DiscordEmbed(title='User Alt Detected',
                             description=f'{original_user.mention} ({original_user.id} is attempting to verify from an '
                                         'alt account.')
        embed.set_author(name=str(original_user), icon_url=original_user.avatar_url_as(format='png'))
        embed.add_field(name='Duplicate Account', value=f'{verification_user.mention} {verification_user} '
                                                        f'({verification_user.id})', inline=False)

        await channel.send(embed=embed)

        return web.json_response({'error': 'Sorry, but duplicate accounts are not allowed. If you feel that '
                                           'this is a mistake, please contact a head moderator or admin.'},
                                 status=403, dumps=ujson.dumps)

    # All checks passed

    session = await new_session(request)
    session['verification'] = {
        'id': verification.user_id,
        'expiration': verification.expiration.timestamp()
    }

    return web.json_response({}, status=204)


@routes.post('/verify')
async def verify(request):
    bot = request.app['bot']
    data = await request.json()

    # Check if the request contains the captcha response
    try:
        response_token = data['token']
    except KeyError:
        return web.json_response({}, status=400)

    session = await get_session(request)

    if 'verification' not in session:
        logger.debug('Verification not in session object, apparently.')
        return web.json_response({}, status=403)

    # Check expiration
    timestamp = session['verification']['expiration']
    expiration = datetime.utcfromtimestamp(timestamp)

    if datetime.utcnow() > expiration:
        return web.json_response({'error': 'Sorry, but the verification link has expired.'}, status=400)

    user = session['verification']['id']
    member = bot.guild.get_member(user)

    if member is None:
        return web.json_response({'error': 'Sorry, but you need to be inside of the server in order to verify your '
                                           'account.'}, status=403)

    # Check if the captcha is ok
    async with ClientSession(json_serialize=ujson.dumps) as request:
        resp = await request.post(url='https://www.google.com/recaptcha/api/siteverify',
                                  data={'secret': RECAPTCHA_SECRET, 'response': response_token})
        data = await resp.json(loads=ujson.loads)

        if data['success']:
            # Remove their role and notify them
            await Verification.update.values(completed=True, key=None).where(Verification.user_id == user).gino.status()

            try:
                await member.remove_roles(DiscordObject(id=VERIFICATION_ROLE))
                await member.send('\ud83c\udf08Meowww~!!\ud83c\udf08\nIf you need anything, just msg meh and the staff '
                                  'will see it.  Also, if you want to set your sona, message me `.setsona` and it will '
                                  'be shared on the server!!! You can also type `.sona @User#1231` in any channel to '
                                  'see their bio \ud83d\ude04\n\ud83c\udfe9\ud83c\udfe9\ud83c\udfe9\ud83c\udfe9\ud83c'
                                  '\udfe9\ud83c\udfe9\ud83c\udfe9\ud83c\udfe9\ud83c\udfe9\ud83c\udfe9\nMsg `.help` for '
                                  'full command list')

                return web.json_response({})

            except DiscordException as err:
                logger.error(f'There was a problem handling Discord stuff: {err}')

        else:
            attempts = await Verification.update.values(attempts=Verification.attempts + 1) \
                .where(Verification.user_id == user).returning(Verification.attempts).gino.scalar()

            if attempts >= MAX_VERIFICATION_ATTEMPTS:
                channel = bot.guild.get_channel(VERIFICATION_CHANNEL)
                embed = DiscordEmbed(title='Max Attempts Exceeded',
                                     description='The following user has failed to successfully complete the captcha '
                                                 f'**{MAX_VERIFICATION_ATTEMPTS}** times.',
                                     color=DiscordColor.red())
                embed.set_author(name=str(member), icon_url=member.avatar_url_as(format='png'))
                embed.add_field(name='User', value=f'{member.mention} {member} ({member.id})', inline=False)

                await channel.send(embed=embed)

                try:
                    await member.send('Hi! I regret to inform you that you\'ve exceeded the maximum amount '
                                      'of verification attempts you\'re alotted, meaning you\'ve failed the '
                                      f'captcha at least **{MAX_VERIFICATION_ATTEMPTS}** times')
                except DiscordException:
                    pass  # We tried.

                return web.json_response({'error': 'Uh oh.. Max attempts exceeded. Please contact a head mod or '
                                                   'administrator for assistance.'}, status=400)


@routes.get('/staff')
async def staff(request):
    bot = request.app['bot']
    roles_ = {
        618688732274360320: 'prince',
        654124833793245186: 'duke',
        598973788670918689: 'nobility',
        598918732387188757: 'guard',
        598918754482782243: 'knight',
        598977299534315546: 'squire'
    }

    roles = {
        'prince': [],
        'duke': [],
        'nobility': [],
        'guard': [],
        'knight': [],
        'squire': []
    }

    async with db.transaction():
        async for user in db.select([Stats.user_id, Profile.avatar]) \
                .where(Profile.user_id == Stats.user_id).gino.iterate():
            # Fetch user via the bot
            member = bot.guild.get_member(user.user_id)
            if not member:
                continue  # Yikes

            for role in member.roles:
                if role.id in roles_:
                    name = roles_[role.id]
                    roles[name].append({
                        'id': user.user_id,
                        'username': str(member),
                        'avatar': user.avatar
                    })
                    break

    return web.json_response(data=roles, dumps=ujson.dumps)


@routes.get(r'/leaderboard/levels/{page:\d+}')
# TODO: Fetch user via await fetch_user if they aren't in the server?
async def levels(request):
    try:
        page = int(request.match_info['page'])
    except ValueError:
        return web.json_response([], status=400)

    if page < 1:
        page = 1

    offset = page * 50
    bot = request.app['bot']

    levels_ = request.app['levels'].items()
    board = dict(itertools.islice(levels_, offset))

    users = []

    for id_, details in board.items():
        member = bot.guild.get_member(id_)

        if member is None:
            continue  # They left before the leaderboard was refreshed.

        position, level, points = details

        users.append({
            'rank': position,
            'level': level,
            'points': points,
            'user': str(member)
        })

    return web.json_response(users, dumps=ujson.dumps)


@routes.get('/leaderboard/levels/count')
async def levelscount(request):
    levels_ = request.app['levels']

    return web.json_response(len(levels_), dumps=ujson.dumps)


@routes.get(r'/leaderboard/crowns/{page:\d+}')
# TODO: Fetch user via await fetch_user if they aren't in the server?
async def crowns(request):
    try:
        page = int(request.match_info['page'])
    except ValueError:
        return web.json_response([], status=400)

    if page < 1:
        page = 1

    offset = page * 50
    bot = request.app['bot']

    crowns_ = request.app['crowns'].items()
    board = dict(itertools.islice(crowns_, offset))

    users = []
    for id_, details in board.items():
        member = bot.guild.get_member(id_)

        if member is None:
            continue  # They left before the leaderboard was refreshed.

        position, amount = details

        users.append({
            'rank': position,
            'crowns': amount,
            'user': str(member)
        })

    return web.json_response(users, dumps=ujson.dumps)


@routes.get('/leaderboard/crowns/count')
async def crownscount(request):
    return web.json_response(len(request.app['crowns']), dumps=ujson.dumps)


@routes.get('/me/panel-info')
async def panelinfo(request):
    session = await get_session(request)

    if 'user' not in session:
        return web.json_response({}, status=400)

    id_ = int(session['user']['id'])
    levels_ = request.app['levels']
    crowns_ = request.app['crowns']

    # (position, level, user.points)
    level_position, level, exp = levels_[id_]
    crown_position, crowns_total = crowns_[id_]

    return web.json_response({
        'level': {
            'pos': level_position,
            'level': level,
            'exp': exp
        },
        'crowns': {
            'pos': crown_position,
            'total': crowns_total
        }
    }, dumps=ujson.dumps)


@routes.post('/submit-sona')
async def submitsona(request):
    # Check if the user is underage or not.
    # Check if values are valid
    # Rate-limit
    session = await get_session(request)

    if 'user' not in session:
        return web.json_response(None, status=400)

    bot = request.app['bot']
    id_ = int(session['user']['id'])
    member = bot.guild.get_member(id_)

    if member is None:
        return web.json_response({}, status=400)

    data = await request.json(loads=ujson.loads)
    logger.debug(f'Received fursona submission: {data}')
    now = datetime.utcnow()

    try:
        data['age'] = int(data['age'])
        data['height'] = int(data['height'])
        data['weight'] = int(data['weight'])

        if data['nsfw'] and data['age'] < 18:
            return web.json_response({}, status=400)  # Underage characters are not allowed to be NSFW.

        fursona_values = dict(discord_id=id_, name=data['name'], gender=data['gender'], age=data['age'],
                              species=data['species'], orientation=data['orientation'],
                              height=data['height'], weight=data['weight'], bio=data['bio'],
                              image=data['image'], verified=False, colour=data['colour'],
                              nsfw=data['nsfw'], index=data['index'], created=now)

        insert_fursona = insert(Fursona).values(**fursona_values)
        do_update_fursona = insert_fursona.on_conflict_do_update(index_elements=[Fursona.discord_id, Fursona.index],
                                                                 set_=fursona_values)

        await do_update_fursona.gino.status()

    except (KeyError, GinoException, PostgresError) as err:
        logger.error(f'Error submitting sona: {err}')
        return web.json_response({}, status=400)

    channel: TextChannel = bot.get_channel(NSFW_MODMAIL_CHANNEL if data['nsfw'] else MODMAIL_CHANNEL)
    feet, inches = (data['height'] // 12, data['height'] % 12)

    embed = DiscordEmbed(color=data['colour'], timestamp=now)
    embed.add_field(name='Name', value=data['name'])
    embed.add_field(name='Gender', value=data['gender'])
    embed.add_field(name='Age', value=data['age'] if data['age'] < 2147483647 else 'Infinite')
    embed.add_field(name='Species', value=data['species'])

    if data['nsfw']:
        embed.add_field(name='Orientation', value=data['orientation'])

    embed.add_field(name='Height', value=f'{feet}\'{inches}"')
    embed.add_field(name='Weight', value=f'{data["weight"]} lbs')

    if data['bio']:
        embed.add_field(name='Bio', value=data['bio'], inline=False)

    if data['image']:
        embed.set_image(url=data['image'])

    embed.set_footer(text=f'Fursona #{data["index"]}')
    embed.set_author(name=str(member) if member.display_name == member.name else f'{member.display_name} ({member})',
                     icon_url=member.avatar_url_as(format='png'))

    def _check(_message):
        # If the message has an embed, mentions the user and has this sona in it.
        return _message.embeds and str(member.id) in _message.content and \
               f'#{data["index"]}' in _message.embeds[0].footer.text

    # First delete old versions of this person's sona
    await channel.purge(limit=15, check=_check)

    client = request.app['client']
    await client.write({'e': 'uuf', 'd': (member.id, data['index'])})

    try:
        message = await channel.send(f'{member.mention}\'s fursona', embed=embed)

        for emoji in ('✅', '🔴', '😈'):
            await message.add_reaction(emoji)

    except DiscordException:
        # Likely due to the character limit being reached.
        return web.json_response({'error': 'Sorry! There was an error submitting your fursona. This is possibly due to '
                                           'there being too many characters/letters in it. Please make sure the '
                                           'overall character count is below 6,000.'}, status=400)

    return web.json_response({})


@routes.post('/contact')
async def contact(request):
    sheet = request.app['sheet']
    loop = asyncio.get_running_loop()

    data = await request.json(loads=ujson.loads)
    logger.debug(f'Received contact form data: {data}')

    try:
        user = data['u']
        unix = int(data['d'])
        date = datetime.utcfromtimestamp(unix).strftime('%c')
        message = data['m']
        token = data['t']

        async with ClientSession(json_serialize=ujson.dumps) as session:
            resp = await session.post(url='https://www.google.com/recaptcha/api/siteverify',
                                      data={'secret': INVIS_CAPTCHA_SECRET, 'response': token})
            data = await resp.json(loads=ujson.loads)

            if not data['success']:
                raise ValueError

    except (KeyError, ValueError):
        return web.json_response({}, status=400)

    try:
        await loop.run_in_executor(None, sheet.append_row, [user, date, message])
    except GSpreadException:
        refresher = request.app['refresh_sheet']
        await refresher(request.app)

        # Try again :3
        await loop.run_in_executor(None, sheet.append_row, [user, date, message])

    return web.json_response({})
